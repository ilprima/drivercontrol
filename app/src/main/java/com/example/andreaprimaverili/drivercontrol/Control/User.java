package com.example.andreaprimaverili.drivercontrol.Control;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB.UserRegisteredDBOpenHelper;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB.UserRegisteredEntity;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by andreaprimaverili on 29/10/2018.
 */

public class User {


    public int id;
    public String name;
    public int limit;
    public String playerId;
    public int controlled; // 1 = utente controllato, 0 = utente che ti controlla

    public User(int id, String name, int limit, String playerId, int controlled) {

        this.id = id;
        this.name = name;
        this.limit = limit;
        this.playerId = playerId;
        this.controlled = controlled;
    }


    //get the 2 users from the DB
    static public ArrayList<User> getUsers(Context activity) {
        UserRegisteredDBOpenHelper dbOpenHelper = new UserRegisteredDBOpenHelper(activity);
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor c = db.query(UserRegisteredEntity.UserRegistered.TABLE, null,
                null, null, null, null, null);
        int idIndex = c.getColumnIndex(UserRegisteredEntity.UserRegistered._ID);
        int nameIndex = c.getColumnIndex(UserRegisteredEntity.UserRegistered.NAME);
        int playerIdIndex = c.getColumnIndex(UserRegisteredEntity.UserRegistered.PLAYERID);
        int controlledIndex = c.getColumnIndex(UserRegisteredEntity.UserRegistered.CONTROLLED);
        int limitIndex = c.getColumnIndex(UserRegisteredEntity.UserRegistered.LIMIT);


        ArrayList<User> users = new ArrayList<User>();

        while (c.moveToNext()) {
            int id = c.getInt(idIndex);
            String n = c.getString(nameIndex);
            int limitU = c.getInt(limitIndex);
            String playerId = c.getString(playerIdIndex);
            int controlled = c.getInt(controlledIndex);
            users.add(new User(id, n, limitU, playerId, controlled));
        }
        String u = "";

        for (User d : users) {
            u += d.name + " ";
        }

        Log.d("debug", "UserId USERS:" + u);


        return users;

    }

    static public User getUserControlled(ArrayList<User> users) {
        if (users != null) {
            for (User u : users) {
                if (u.controlled == 1) {
                    return u;
                }
            }
        }
        return null;
    }

    static public User getUserThatControllYou(ArrayList<User> users) {
        if (users != null) {
            for (User u : users) {
                if (u.controlled == 0) {
                    return u;
                }
            }
        }
        return null;
    }
}
