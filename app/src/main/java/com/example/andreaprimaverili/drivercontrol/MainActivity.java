package com.example.andreaprimaverili.drivercontrol;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;

import com.example.andreaprimaverili.drivercontrol.Control.ControlFragment;
import com.example.andreaprimaverili.drivercontrol.Driver.DriverFragment;
import com.example.andreaprimaverili.drivercontrol.Driver.HistoryFragment;
import com.example.andreaprimaverili.drivercontrol.Driver.MapTools.Point;
import com.example.andreaprimaverili.drivercontrol.Notification.NotificationOpenHandler;
import com.example.andreaprimaverili.drivercontrol.Notification.NotificationService;
import com.github.mikephil.charting.data.Entry;
import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {


    public Menu menu;
    boolean connected;

    static MainActivity instance;

    //store the data for the graph
    public List<Entry> queueHisotry = new ArrayList<Entry>();
    public int current_index = 1;
    //store the data for the map
    public List<Point> points = new ArrayList<Point>();



    LocationTrackingService trackingService;

    //salvo l'istanza del driver fragment ma non del control fragment perchè
    // ogni volta voglio che venga ricaricata la lista delle notifiche dal db

    private DriverFragment df;
    private ControlFragment cf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //to be able to use MainActivity variables in service
        instance = this;

        //init history queue

        if (queueHisotry == null)
            queueHisotry = new ArrayList<Entry>();

        // turn your data into Entry objects
        queueHisotry.add(new Entry(current_index, 0));


        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.InAppAlert)
                .setNotificationOpenedHandler(new NotificationOpenHandler(this))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);


        //the first time insert the first fragment
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        this.df = new DriverFragment();
        fragmentTransaction.add(R.id.fragmentContainer, df);
        fragmentTransaction.commit();

        startService(new Intent(this, LocationTrackingService.class));
        startService(new Intent(this, NotificationService.class));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent calcServInt = new Intent(this, LocationTrackingService.class);
        bindService(calcServInt, conn, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (connected) {
            unbindService(conn);
            connected = false;
        }
    }


    public ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationTrackingService.TrackingBinder binder = (LocationTrackingService.TrackingBinder) service;
            trackingService = binder.getService();
            connected = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            connected = false;
        }
    };


    public boolean onOptionsItemSelected(MenuItem item) {

        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;

        ActionMenuItemView btn1 = findViewById(R.id.action_home);
        ActionMenuItemView btn2 = findViewById(R.id.action_control);

        switch (item.getItemId()) {
            case R.id.action_home:

                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                if (df == null)
                    df = new DriverFragment();
                fragmentTransaction.replace(R.id.fragmentContainer, df);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.action_home_active));
                menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_action_name));

                return true;

            case R.id.action_control:

                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                if (cf == null)
                    cf = new ControlFragment();
                fragmentTransaction.replace(R.id.fragmentContainer, cf);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.action_home));
                menu.getItem(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_action_name_active));

                return true;
        }
        return false;
    }

}
