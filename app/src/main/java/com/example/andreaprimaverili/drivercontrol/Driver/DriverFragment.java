package com.example.andreaprimaverili.drivercontrol.Driver;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.andreaprimaverili.drivercontrol.Control.User;
import com.example.andreaprimaverili.drivercontrol.MainActivity;
import com.example.andreaprimaverili.drivercontrol.R;

import java.util.ArrayList;

/**
 * Created by andreaprimaverili on 14/10/2018.
 */

public class DriverFragment extends Fragment implements View.OnClickListener {

    private TextView speedBoxText;
    private TextView limitBoxText;
    private View myView;

    private float currentSpeed = 0;

    private UpdateBroadcastReceiver ubr;
    private IntentFilter filter;


    private HistoryFragment hf;
    private MapViewFragment mf;

    private ArrayList<User> users;
    private User controllingUser;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.activity_driver, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        myView = getView();

        speedBoxText = (TextView) myView.findViewById(R.id.speed);
        speedBoxText.setText(currentSpeed + "");
        limitBoxText = (TextView) myView.findViewById(R.id.limit);
        limitBoxText.setText("nessun limite");
        limitBoxText.setTextColor(getResources().getColor(R.color.colorKmLow));

        Button b1 = (Button) myView.findViewById(R.id.btnHistory);
        Button b2 = (Button) myView.findViewById(R.id.btnMap);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);


        //not active
        b1.setBackgroundColor(getResources().getColor(R.color.colorDarkBlue));
        b1.setTextColor(getResources().getColor(R.color.colorKmLow));

        ubr = new UpdateBroadcastReceiver();
        filter = new IntentFilter(UpdateBroadcastReceiver.LOCATION_READY_ACTION);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //the first time insert the first fragment

        mf = new MapViewFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.fragmentDriverSubContainer, mf)
                .commit();
    }


    @Override
    public void onResume() {
        super.onResume();

        //set the navbar btn active
        if (((MainActivity)getActivity()).menu !=null) {
            ((MainActivity) getActivity()).menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.action_home_active));
            ((MainActivity) getActivity()).menu.getItem(1).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_action_name));
        }

        users = User.getUsers(getActivity());
        controllingUser = User.getUserThatControllYou(users);
        speedBoxText.setText(currentSpeed + "");

        if (controllingUser!= null){
            limitBoxText.setText(controllingUser.name + " " + controllingUser.limit + " Km/h");
            limitBoxText.setTextColor(getResources().getColor(R.color.colorKmHigh));
        } else {
            limitBoxText.setText("nessun limite");
            limitBoxText.setTextColor(getResources().getColor(R.color.colorKmLow));
        }

        getActivity().registerReceiver(ubr, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(ubr);
    }


    @Override
    public void onClick(View v) {

        Button b1 = (Button) myView.findViewById(R.id.btnHistory);
        Button b2 = (Button) myView.findViewById(R.id.btnMap);

        if (v.getId() == R.id.btnHistory) {


            if (hf == null)
                hf = new HistoryFragment();
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fragmentDriverSubContainer, hf)
                    .commit();


            //active
            b1.setBackgroundColor(getResources().getColor(R.color.colorKmLow));
            b1.setTextColor(getResources().getColor(R.color.colorWhite));

            //not active
            b2.setBackgroundColor(getResources().getColor(R.color.colorDarkBlue));
            b2.setTextColor(getResources().getColor(R.color.colorKmLow));


        } else if (v.getId() == R.id.btnMap) {


            if (mf == null)
                mf = new MapViewFragment();
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fragmentDriverSubContainer, mf)
                    .commit();

            //active
            b2.setBackgroundColor(getResources().getColor(R.color.colorKmLow));
            b2.setTextColor(getResources().getColor(R.color.colorWhite));


            //not active
            b1.setBackgroundColor(getResources().getColor(R.color.colorDarkBlue));
            b1.setTextColor(getResources().getColor(R.color.colorKmLow));

        }

    }


    public class UpdateBroadcastReceiver extends BroadcastReceiver {

        public static final String LOCATION_READY_ACTION = "it.unipi.iet.SPEED_READY";

        public void onReceive(Context context, Intent intent) {

            currentSpeed = intent.getFloatExtra("speed", 0);
            speedBoxText.setText(currentSpeed + "");


        }

    }
}

