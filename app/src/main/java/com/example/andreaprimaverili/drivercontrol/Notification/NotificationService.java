package com.example.andreaprimaverili.drivercontrol.Notification;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;


import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;

// This service is necessary to receive notifications in foregrounded apps,
// to receive data payload, to send upstream messages, and so on.


// come fare senza server: https://stackoverflow.com/questions/37435750/how-to-send-device-to-device-messages-using-firebase-cloud-messaging



/*


Onesignal guide: https://documentation.onesignal.com/docs/android-sdk-setup#section-android-studio


User-User Messages: https://documentation.onesignal.com/docs/user-user-messages

API doc: https://documentation.onesignal.com/docs/android-native-sdk






 */

public class NotificationService extends Service {


    private String userID = "";

    @Override
    public void onCreate() {
        // TODO: Actions to perform when service is created.
        super.onCreate();

        //OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.DEBUG);


        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {

                userID = userId;
                Log.d("debug", "UserId:" + userId);

                //save the UserId in the shared preferences
                SharedPreferences notifSettings = getSharedPreferences("NotificationPreferences", MODE_PRIVATE);
                SharedPreferences.Editor prefEditor = notifSettings.edit();
                prefEditor.putString("UserId", userId);
                prefEditor.commit();

                //sendTestNotification();

                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);

            }
        });

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void sendTestNotification() {


        try {
            OneSignal.postNotification(new JSONObject("{'contents': {'en':'Test Message'}, 'include_player_ids': ['" + "35c5fc55-4141-442b-89b2-96b02d31c2b1" + "']}"),
                    new OneSignal.PostNotificationResponseHandler() {
                        @Override
                        public void onSuccess(JSONObject response) {
                            Log.i("debug", "UserId postNotification Success: " + response.toString());
                        }

                        @Override
                        public void onFailure(JSONObject response) {
                            Log.e("debug", "UserId postNotification Failure: " + response.toString());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
