package com.example.andreaprimaverili.drivercontrol.Driver;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.andreaprimaverili.drivercontrol.Control.User;
import com.example.andreaprimaverili.drivercontrol.Driver.DriverFragment;
import com.example.andreaprimaverili.drivercontrol.MainActivity;
import com.example.andreaprimaverili.drivercontrol.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreaprimaverili on 14/10/2018.
 */

public class HistoryFragment extends Fragment {


    private View myView;
    private LineChart chart;
    private LineDataSet dataSet;

    private List<Entry> queue;
    private int current_index = 1;


    private UpdateBroadcastReceiver ubr;
    private IntentFilter filter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        myView = getView();


        /*          Graph doc: https://github.com/PhilJay/MPAndroidChart/wiki/Getting-Started            */

        // in this example, a LineChart is initialized from xml
        chart = (LineChart) myView.findViewById(R.id.chart);

        chart.clear();
        chart.invalidate();


        ubr = new UpdateBroadcastReceiver();
        filter = new IntentFilter(DriverFragment.UpdateBroadcastReceiver.LOCATION_READY_ACTION);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(ubr, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(ubr);
    }


    private void setChart() {

        dataSet = new LineDataSet(queue, "Speed"); // add entries to dataset
        //dataSet.setColor(...);
        //dataSet.setValueTextColor(...);

        dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        dataSet.setDrawFilled(true);
        LineData lineData = new LineData(dataSet);

        // max speed line

        LimitLine ll1 = null;
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines

        ArrayList<User> users = User.getUsers(getActivity());
        User userControlling = User.getUserThatControllYou(users);
        if (userControlling != null) { //add limit line only if there is a limit setted
            ll1 = new LimitLine(userControlling.limit, "Speed limit");
            ll1.setLineWidth(4f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);
            leftAxis.addLimitLine(ll1);
        }


        //styling
        chart.setDrawBorders(false);
        chart.setBorderWidth(0);
        Description d = new Description();
        d.setText("Speed history");
        d.setTextColor(0);
        d.setTextSize(10);
        chart.setDescription(d);
        chart.setNoDataText("Caricamento in corso");

        //x axis

        XAxis x = chart.getXAxis();
        x.setPosition(XAxis.XAxisPosition.BOTTOM);
        x.setTextColor(R.color.colorWhite);
        x.setTextSize(10f);
        x.setDrawAxisLine(true);
        x.setDrawGridLines(false);
        x.setAxisLineColor(R.color.colorWhite);
        x.setEnabled(false);

        //x axis

        YAxis y = chart.getAxisLeft();
        y.setTextColor(R.color.colorWhite);
        y.setTextSize(10f);
        y.setDrawAxisLine(true);
        y.setDrawGridLines(true);
        y.setAxisLineColor(R.color.colorWhite);


        chart.setData(lineData);
        chart.invalidate(); // refresh


    }


    public class UpdateBroadcastReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

            queue = ((MainActivity) getActivity()).queueHisotry;
            setChart();

        }

    }


}
