package com.example.andreaprimaverili.drivercontrol.Driver.MapTools;

import android.app.Activity;
import android.graphics.Color;

import com.example.andreaprimaverili.drivercontrol.Control.User;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreaprimaverili on 16/10/2018.
 */

public class PointManager {
    public static String TAG = "PointManager";
    private static final int TRAIL_SIZE = 5;
    private List<Point> queue = new ArrayList<Point>();

    public void insert(Point p) {
        queue.add(p);
        if (queue.size() > TRAIL_SIZE) queue.remove(0);
    }

    public PolylineOptions getPolylineOptions(float speed) {
        PolylineOptions po = new PolylineOptions();
        for (Point p : queue)
            po.add(new LatLng(p.getLatitude(), p.getLongitude()));
        if (speed < 10)
            po.color(Color.GREEN);
        else if (speed >= 10 && speed < 50)
            po.color(Color.YELLOW);
        else if (speed >= 50 && speed < 100)
            po.color(Color.RED);
        else
            po.color(Color.MAGENTA);

        return po;
    }

    public PolylineOptions getPolylineOptionStepByStep(Point p, Point p2, Point p3, Activity activity) {
        PolylineOptions po = new PolylineOptions();

        po.add(new LatLng(p.getLatitude(), p.getLongitude()));
        po.add(new LatLng(p2.getLatitude(), p2.getLongitude()));
        po.add(new LatLng(p3.getLatitude(), p3.getLongitude()));

        float maxSpeed = p.getSpeed() > p2.getSpeed() ? p.getSpeed() : p2.getSpeed();
        maxSpeed =  maxSpeed > p3.getSpeed() ? maxSpeed : p3.getSpeed();

        float limitLow = 20, limitMed = 50, limitHigh = 100;

        ArrayList<User> users = User.getUsers(activity);
        User userControlling = User.getUserThatControllYou(users);

        if (userControlling != null){
            limitLow = userControlling.limit / 3;
            limitMed = userControlling.limit * 2 / 3;
            limitHigh = userControlling.limit;
        }

        if (maxSpeed < limitLow)
            po.color(Color.GRAY);
        else if (maxSpeed >= limitLow && maxSpeed < limitMed)
            po.color(Color.GREEN);
        else if (maxSpeed >= limitMed && maxSpeed < limitHigh)
            po.color(Color.YELLOW);
        else
            po.color(Color.RED);

        return po;
    }

    public List<CircleOptions> getPositions() {
        List<CircleOptions> co = new ArrayList<CircleOptions>();
        for (Point p : queue)
            co.add(new CircleOptions()
                    .center(new LatLng(p.getLatitude(), p.getLongitude()))
                    .radius(p.getAccuracy())
                    .strokeColor(Color.BLUE));
        return co;
    }

    public MarkerOptions getLastPositionMarker() {
        if (queue.size() == 0)
            return null;
        MarkerOptions mo = new MarkerOptions();
        Point last = getLastPoint();
        mo.position(new LatLng(last.getLatitude(), last.getLongitude()));
        return mo;
    }

    public Point getLastPoint() {
        if (queue.size() == 0)
            return null;
        Point last = queue.get(queue.size() - 1);
        return last;
    }

    public void deletePoints() {
        queue.clear();
    }
}
