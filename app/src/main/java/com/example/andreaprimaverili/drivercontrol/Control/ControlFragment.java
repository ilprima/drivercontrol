package com.example.andreaprimaverili.drivercontrol.Control;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB.NotificationDBOpenHelper;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB.NotificationSpeedEntity;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB.UserRegisteredDBOpenHelper;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB.UserRegisteredEntity;
import com.example.andreaprimaverili.drivercontrol.MainActivity;
import com.example.andreaprimaverili.drivercontrol.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by andreaprimaverili on 14/10/2018.
 */

/*
*
* QRCode guide: https://github.com/journeyapps/zxing-android-embedded
*
*
* */

public class ControlFragment extends Fragment implements View.OnClickListener {

    public class SpeedLimit {

        String location;
        int speed;
        String user;
        String time;
        int limit;

        SpeedLimit(String location, int speed, String user, String time, int limit) {

            this.location = location;
            this.speed = speed;
            this.user = user;
            this.time = time;
            this.limit = limit;
        }
    }


    ArrayList<SpeedLimit> list;
    ArrayList<User> users;

    private Activity currentActivity;
    private Context applicationContext;

    private ListSpeedOverlimitedAdapter listSpeedOverlimitedAdapter;

    private String myUserId = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_control, container, false);
        applicationContext = getContext();
        currentActivity = getActivity();


        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        //set the navbar btn active
        if (((MainActivity) getActivity()).menu != null) {
            ((MainActivity) getActivity()).menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.action_home));
            ((MainActivity) getActivity()).menu.getItem(1).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_action_name_active));
        }

        NotificationDBOpenHelper dbOpenHelper = new NotificationDBOpenHelper(getActivity());
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor c = db.query(NotificationSpeedEntity.NotificationEntity.TABLE, null,
                null, null, null, null,
                NotificationSpeedEntity.NotificationEntity.TIME + " DESC");
        int locationIndex = c.getColumnIndex(NotificationSpeedEntity.NotificationEntity.LOCATION);
        int userIndex = c.getColumnIndex(NotificationSpeedEntity.NotificationEntity.USER);
        int timeIndex = c.getColumnIndex(NotificationSpeedEntity.NotificationEntity.TIME);
        int speedIndex = c.getColumnIndex(NotificationSpeedEntity.NotificationEntity.SPEED);
        int limitIndex = c.getColumnIndex(NotificationSpeedEntity.NotificationEntity.LIMIT);


        list = new ArrayList<SpeedLimit>();


        while (c.moveToNext()) {
            String location = c.getString(locationIndex);
            int speed = c.getInt(speedIndex);
            String user = c.getString(userIndex);
            String time = c.getString(timeIndex);
            int limit = c.getInt(limitIndex);

            list.add(new SpeedLimit(location, speed, user, time, limit));
        }


        listSpeedOverlimitedAdapter = new ListSpeedOverlimitedAdapter(getFragmentManager(), applicationContext, R.layout.speed_overlimited_row, list);
        ListView listView = getView().findViewById(R.id.notification_list);
        listView.setAdapter(listSpeedOverlimitedAdapter);

        c.close();

        //handle PlayerID
        SharedPreferences notifSettings = getActivity().getSharedPreferences("NotificationPreferences", MODE_PRIVATE);
        myUserId = notifSettings.getString("UserId", "");
        Log.d("debug", "UserId retrive:" + myUserId);
       /* TextView mTextMessage = (TextView) getView().findViewById(R.id.Dr);
        mTextMessage.setText(userId);
*/

        //QR code
        try {

            ImageView imageViewQrCode = (ImageView) getActivity().findViewById(R.id.qrCode);

            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(myUserId, BarcodeFormat.QR_CODE, 200, 200);
            imageViewQrCode.setImageBitmap(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ImageButton btn = getActivity().findViewById(R.id.btnScanner);
        btn.setOnClickListener(this);

        users = User.getUsers(getActivity());

        updateUsersLabel();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnScanner:
                this.openQRCodeScanner();
                break;

            case R.id.deleteControlled:
                this.deleteControlled();
                break;

            default:
                break;
        }
    }

    public void openQRCodeScanner() {
        IntentIntegrator.forFragment(this)
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
                .setOrientationLocked(true)
                .initiateScan();
    }


    // Get the results of the Qr code Scanner
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) { //cancelled
            if (result.getContents() == null) {
                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();
            } else { // brcode scanned
                Toast.makeText(getActivity(), "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();

                final String userId = result.getContents();

                //apri il dialog per inserire il nome dell'utente da controllare

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Inserisci il nome e limite");

                Context context = getView().getContext();
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                // Add a TextView here for the "Name" label
                final EditText nameBox = new EditText(context);
                nameBox.setHint("Nome utente da controllare");
                nameBox.setInputType(InputType.TYPE_CLASS_TEXT);
                layout.addView(nameBox); // Notice this is an add method

                // Add a TextView here for the "Name" label
                final EditText yourNameBox = new EditText(context);
                yourNameBox.setHint("Il tuo nome da mostrare");
                yourNameBox.setInputType(InputType.TYPE_CLASS_TEXT);
                layout.addView(yourNameBox); // Notice this is an add method

                // Add another TextView here for the "Limit" label
                final EditText limitBox = new EditText(context);
                limitBox.setHint("limite in Km/h");
                limitBox.setInputType(InputType.TYPE_CLASS_NUMBER);

                layout.addView(limitBox); // Another add method

                builder.setView(layout); // Again this is a set method, not add

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        insertNewUserToControl(yourNameBox.getText().toString(), nameBox.getText().toString(), userId, limitBox.getText().toString());

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    //register a new user to be controlled
    public void insertNewUserToControl(String yourName, String name, String userId, String limit) {

        UserRegisteredDBOpenHelper dbOpenHelper = new UserRegisteredDBOpenHelper(getActivity());
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();

        users = User.getUsers(getActivity());

        if (yourName != null && yourName.compareTo("") != 0 && name != null && name.compareTo("") != 0 && userId != null && userId.compareTo("") != 0 && limit != null && limit.compareTo("") != 0) {

            User controlled = User.getUserControlled(users);
            ContentValues values = new ContentValues();
            values.put(UserRegisteredEntity.UserRegistered.NAME, name);
            values.put(UserRegisteredEntity.UserRegistered.LIMIT, limit);
            values.put(UserRegisteredEntity.UserRegistered.CONTROLLED, 1);
            values.put(UserRegisteredEntity.UserRegistered.PLAYERID, userId);

            long r = -1;
            if (controlled == null) { // nessun utente attualamente controllato

                r = db.insert(UserRegisteredEntity.UserRegistered.TABLE, null, values);

            } else { //c'è già un utente controllato

                r = db.update(UserRegisteredEntity.UserRegistered.TABLE, values, UserRegisteredEntity.UserRegistered.CONTROLLED + "=?", new String[]{"1"});

            }

            if (r != -1) {
                updateUsersLabel();
                sendNotificationNewControl(yourName);
            }

        } else { //dati non completi
            Toast.makeText(getActivity(), "Alcuni dati sono errati o incompleti ", Toast.LENGTH_LONG).show();
        }

    }


    public void updateUsersLabel() {

        users = User.getUsers(getActivity());

        TextView personControlled = getView().findViewById(R.id.personControlled);
        User userControlled = User.getUserControlled(users);

        TextView personControlling = getView().findViewById(R.id.personControlling);
        User userControlling = User.getUserThatControllYou(users);

        if (userControlled == null) {
            personControlled.setText("-");
            getView().findViewById(R.id.deleteControlled).setVisibility(View.INVISIBLE);
        } else {
            personControlled.setText(userControlled.name + " " + userControlled.limit + "Km/h");
            getView().findViewById(R.id.deleteControlled).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.deleteControlled).setOnClickListener(this);
        }

        if (userControlling == null)
            personControlling.setText("-");
        else {
            personControlling.setText(userControlling.name + " " + userControlling.limit + "Km/h");
        }

    }

    public void sendNotificationNewControl(String yourName) {

        User userControlled = User.getUserControlled(users);

        try {
            OneSignal.postNotification(
                    new JSONObject("{'contents': {'en':'Nuova controllo attivato'}, " +
                            "'include_player_ids': ['" + userControlled.playerId + "']," +
                            "'data': {'name': '" + yourName + "', 'limit': '" + userControlled.limit + "','playerId': '" + myUserId + "' }}"), null);

            Log.d("debug", "UserId json body: " + "{'contents': {'en':'Nuova controllo attivato'}, " +
                    "'include_player_ids': ['" + userControlled.playerId + "']," +
                    "'data': {'name': '" + yourName + "', 'limit': '" + userControlled.limit + "','playerId': '" + userControlled.playerId + "' }}");


            Toast.makeText(getActivity(), "Utente aggiunto con successo ", Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("debug", "UserId EXCEPTION: " + e.getMessage());
            Toast.makeText(getActivity(), "Errore nell'aggiuta dell'utente, riprova", Toast.LENGTH_LONG).show();

        }
    }

    public void deleteControlled() {

        UserRegisteredDBOpenHelper dbOpenHelper = new UserRegisteredDBOpenHelper(getActivity());
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();

        User controlled = User.getUserControlled(users);

        long r = -1;
        if (controlled != null) { // nessun utente attualamente controllato

            r = db.delete(UserRegisteredEntity.UserRegistered.TABLE, UserRegisteredEntity.UserRegistered.CONTROLLED + "=?", new String[]{"1"});

            if (r != -1) {
                updateUsersLabel();
                try {
                    OneSignal.postNotification(
                            new JSONObject("{'contents': {'en':'Controllo disattivato'}, " +
                                    "'include_player_ids': ['" + controlled.playerId + "']," +
                                    "'data': {'disabled': '" + 1 + "'}}"), null);
                    Toast.makeText(getActivity(), "Utente eliminato con successo ", Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("debug", "UserId EXCEPTION: " + e.getMessage());
                    Toast.makeText(getActivity(), "Errore nell'eliminazione dell'utente, riprova", Toast.LENGTH_LONG).show();

                }
            }
        }

    }


}




