package com.example.andreaprimaverili.drivercontrol.Notification;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;

import com.example.andreaprimaverili.drivercontrol.Control.ControlFragment;
import com.example.andreaprimaverili.drivercontrol.Control.User;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB.NotificationDBOpenHelper;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB.NotificationSpeedEntity;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB.UserRegisteredDBOpenHelper;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB.UserRegisteredEntity;
import com.example.andreaprimaverili.drivercontrol.MainActivity;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by andreaprimaverili on 28/10/2018.
 */

public class NotificationOpenHandler implements OneSignal.NotificationOpenedHandler, OneSignal.NotificationReceivedHandler {


    private MainActivity application;

    private ArrayList<User> users;

    public NotificationOpenHandler(MainActivity application) {
        this.application = application;
    }

    //notificationOpened : This will be called when a notification is tapped on.

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        notifArrived(result.notification);

        // React to button pressed
        OSNotificationAction.ActionType actionType = result.action.type;
        if (actionType == OSNotificationAction.ActionType.ActionTaken)
            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);

        // Launch new activity using Application object
        startApp();
    }

    //notificationReceived : This will be called when a notification is received while your app is running.
    @Override
    public void notificationReceived(OSNotification notification) {

        notifArrived(notification);

        //not launch the app

        // Launch new activity using Application object
        startApp();
    }


    private void notifArrived(OSNotification notification) {
        Log.d("debug", "UserId: ARRIVATA");
        users = User.getUsers(application);

        // Get custom datas from notification
        JSONObject data = notification.payload.additionalData;
        if (data != null) {

            String name = data.optString("name", null);
            String limit = data.optString("limit", null);
            String playerId = data.optString("playerId", null);
            String speed = data.optString("speed", null);
            String location = data.optString("location", null);
            String timestamp = data.optString("timestamp", null);
            String disabled = data.optString("disabled", null);

            if (playerId != null) { //case notifica per nuovo controllo
                Log.d("debug", "UserId: ARRIVATA NUOVO CONTROLLO");
                insertNewUserControllingMe(name, playerId, limit);
            } else if (speed != null) {
                Log.d("debug", "UserId: ARRIVATA LIMITE SUPERATO");
                insertNewNotificationDB(speed, location, timestamp);
            } else if (disabled != null) {
                Log.d("debug", "UserId: ARRIVATA ELIMINA CONTROLLO");
                deleteControlling();
            }

        }
    }

    private void startApp() {
        Log.d("debug", "OPEN_NOTIF: startApp");

        Intent intent = new Intent(application, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        application.startActivity(intent);
    }


    //register a new user that controll you
    public void insertNewUserControllingMe(String name, String userId, String limit) {

        UserRegisteredDBOpenHelper dbOpenHelper = new UserRegisteredDBOpenHelper(application);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();

        users = User.getUsers(application);

        User controlling = User.getUserThatControllYou(users);
        ContentValues values = new ContentValues();
        values.put(UserRegisteredEntity.UserRegistered.NAME, name);
        values.put(UserRegisteredEntity.UserRegistered.LIMIT, limit);
        values.put(UserRegisteredEntity.UserRegistered.CONTROLLED, 0);
        values.put(UserRegisteredEntity.UserRegistered.PLAYERID, userId);

        long r = -1;

        if (controlling == null) { // nessun utente mi controlla attualmente

            r = db.insert(UserRegisteredEntity.UserRegistered.TABLE, null, values);

        } else { //c'è già un utente che mi controlla

            r = db.update(UserRegisteredEntity.UserRegistered.TABLE, values, UserRegisteredEntity.UserRegistered.CONTROLLED + "=?", new String[]{"0"});

        }

        if (r != -1) {
            Toast.makeText(application, "L'utente " + name + " adesso ti controlla", Toast.LENGTH_LONG).show();
        }

    }


    public void deleteControlling() {

        UserRegisteredDBOpenHelper dbOpenHelper = new UserRegisteredDBOpenHelper(application);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();

        User controlling = User.getUserThatControllYou(users);

        long r = -1;
        if (controlling != null) { // nessun utente attualamente controllato

            r = db.delete(UserRegisteredEntity.UserRegistered.TABLE, UserRegisteredEntity.UserRegistered.CONTROLLED + "=?", new String[]{"0"});

        }
    }


    public void insertNewNotificationDB(String speed, String location, String timestmap) {

        User userControlled = User.getUserControlled(users);

        if (userControlled != null) {

            Log.d("debug", "UserId: controlled diverso da null");


            NotificationDBOpenHelper dbOpenHelper = new NotificationDBOpenHelper(application);
            SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(NotificationSpeedEntity.NotificationEntity.LIMIT, userControlled.limit);
            values.put(NotificationSpeedEntity.NotificationEntity.LOCATION, location);
            values.put(NotificationSpeedEntity.NotificationEntity.SPEED, Math.round(Float.parseFloat(speed)));
            values.put(NotificationSpeedEntity.NotificationEntity.USER, userControlled.name);
            values.put(NotificationSpeedEntity.NotificationEntity.TIME, timestmap);

            long r = db.insert(NotificationSpeedEntity.NotificationEntity.TABLE, null, values);

        }
    }


}


