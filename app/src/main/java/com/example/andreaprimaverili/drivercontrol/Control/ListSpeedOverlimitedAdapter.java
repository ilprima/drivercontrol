package com.example.andreaprimaverili.drivercontrol.Control;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.andreaprimaverili.drivercontrol.MainActivity;
import com.example.andreaprimaverili.drivercontrol.R;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by andreaprimaverili on 21/10/2018.
 */

public class ListSpeedOverlimitedAdapter extends ArrayAdapter<ControlFragment.SpeedLimit> {

    private int resource;

    FragmentManager fragmentManager;

    public ListSpeedOverlimitedAdapter(FragmentManager fragmentManager, @NonNull Context context, int resource, List<ControlFragment.SpeedLimit> speedLimits) {
        super(context, resource, speedLimits);
        this.resource = resource;
        this.fragmentManager = fragmentManager;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout limitsView;


        ControlFragment.SpeedLimit speed_limit = getItem(position);

        if (convertView == null) {
            limitsView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater) getContext().getSystemService(inflater);
            vi.inflate(resource, limitsView, true);
        } else {
            limitsView = (LinearLayout) convertView;
        }
        String displayLimit = speed_limit.speed + "";


        TextView speedTextview = (TextView) limitsView.findViewById(R.id.SpeedTextview);
        speedTextview.setText(displayLimit);

        TextView nameTextview = (TextView) limitsView.findViewById(R.id.userNameview);
        nameTextview.setText(speed_limit.user);
        TextView whereTextview = (TextView) limitsView.findViewById(R.id.whereTextview);
        whereTextview.setText(speed_limit.location);
        TextView limitTextview = (TextView) limitsView.findViewById(R.id.LimitTextview);
        limitTextview.setText("/" + speed_limit.limit);

        String parsedDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            parsedDate = dateFormat.format(new Date(Long.parseLong(speed_limit.time)));

        } catch (Exception e) {
        }

        TextView whenTextview = (TextView) limitsView.findViewById(R.id.whenTextview);
        whenTextview.setText(parsedDate);

        /*
        limitsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MapDialogFragment yourDialogFragment = MapDialogFragment.newInstance(0);
                yourDialogFragment.show(fragmentManager.beginTransaction(), "DialogFragment");


            }
        });
        */

        return limitsView;
    }


}
