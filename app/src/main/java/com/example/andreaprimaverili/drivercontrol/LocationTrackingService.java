package com.example.andreaprimaverili.drivercontrol;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.andreaprimaverili.drivercontrol.Control.User;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB.NotificationDBOpenHelper;
import com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB.NotificationSpeedEntity;
import com.example.andreaprimaverili.drivercontrol.Driver.DriverFragment;
import com.example.andreaprimaverili.drivercontrol.Driver.MapViewFragment;
import com.example.andreaprimaverili.drivercontrol.Driver.MapTools.Point;
import com.example.andreaprimaverili.drivercontrol.Driver.MapTools.PointManager;
import com.github.mikephil.charting.data.Entry;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by andreaprimaverili on 16/10/2018.
 */

public class LocationTrackingService extends Service implements LocationListener {

    private static final int TIME_THRESHOLD = 100;
    private static final int ACCURACY_FRACTION = 10;
    private static final int VELOCITY_THRESHOLD = 200;
    private static final int MIN_TIME_SAMPLE_LOCATION = 0; //minimo tempo ogni quanto campionare: in millisecondi
    private static final int MIN_DIST_SAMPLE_LOCATION = 0; //minima distanza ogni quanto campionare: in metri

    private static final int TRAIL_SIZE = 30;
    private static final int TRAIL_SIZE_LOCATIONS = 5;

    private static final int SPEED_LIMIT = 50;


    private LocationManager locationManager;
    private Location currentLocation = null;
    private float currentSpeed = 0;


    private boolean isMapInitialized = false;


    private final IBinder trackingBinder = new TrackingBinder();

    PointManager pointManager;

    private long lastNotificationSent = 0;
    private float lastNotificationSpeed = 0;

    private ArrayList<User> users;
    private User controllingUser;


    @Override
    public IBinder onBind(Intent intent) {
        // return null in “started” services
        return trackingBinder;
    }


    @Override
    public void onCreate() {
        // TODO: Actions to perform when service is created.
        pointManager = new PointManager();
        super.onCreate();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return Service.START_REDELIVER_INTENT;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_SAMPLE_LOCATION, MIN_DIST_SAMPLE_LOCATION, this);

        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        sendNewLocationIntent();

        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onLocationChanged(Location location) {


        Point lastPoint = pointManager.getLastPoint();
        if (currentLocation == null) { //first time
            currentLocation = location;
            sendNewLocationIntent();
        } else { //not first time
            float currentAccuracy = location.getAccuracy();
            float previousAccuracy = lastPoint.getAccuracy();
            float accuracyDifference = Math.abs(previousAccuracy - currentAccuracy);
            boolean betterAccuracy = currentAccuracy < previousAccuracy;
            boolean lowerAccuracyAcceptable = !betterAccuracy
                    && lastPoint.getProvider().equals(location.getProvider())
                    && (accuracyDifference <= previousAccuracy / ACCURACY_FRACTION);
            float[] results = new float[1];

            Location.distanceBetween(lastPoint.getLatitude(), lastPoint.getLongitude(),
                    location.getLatitude(), location.getLongitude(), results);

            //get the speed
            currentSpeed = Math.round(location.getSpeed() * (3.6)); //convert to Km/h from m/s
            //if ((location.getTime() - lastPoint.getTime()) >=1000){


            if (currentSpeed <= VELOCITY_THRESHOLD && (betterAccuracy || (location.getTime() - lastPoint.getTime()) > TIME_THRESHOLD || lowerAccuracyAcceptable)) {
                currentLocation = location;

                Log.d("debug", "LOCATION_CHANGE " + location.getSpeed());

                checkSpeedLimit();
                sendNewSpeedIntent();
                sendNewLocationIntent();
            } else {
                //Ignore point, maybe is an outlier
            }
        }

    }


    void checkSpeedLimit() {

        users = User.getUsers(this);
        controllingUser = User.getUserThatControllYou(users);

        if (controllingUser != null && currentSpeed >= controllingUser.limit) {

            Long now = new Date().getTime();

            //controllo di non aver già inviato una notifica nei precedenti 10 secondi
            // e che la nuova velocità sia maggiore di almeno 10Km/h
            // per evitare un invio continuo di notifiche
            if (now > lastNotificationSent + 10000 && currentSpeed >= lastNotificationSpeed + 10) {
                //insertNewNotificationDB();
                sendNewNotificationLimitExceeded();
                lastNotificationSent = now;
                lastNotificationSpeed = currentSpeed;
            } else
                //altrimenti se la velocità non è maggiore invio una notifica solo se è passato 1 minuto dall'ultima
                if (now > lastNotificationSent + 60000) {
                    //insertNewNotificationDB();
                    sendNewNotificationLimitExceeded();
                    lastNotificationSent = now;
                    lastNotificationSpeed = currentSpeed;
                }

        }
    }


    //send the broadcast intent with the new location detected and filtered
    void sendNewLocationIntent() {

        if (currentLocation != null) {
            Point point = new Point(currentLocation.getLatitude(), currentLocation.getLongitude(),
                    currentLocation.getAccuracy(), currentLocation.getTime(), currentLocation.getProvider(), currentSpeed, currentLocation.getBearing());
            pointManager.insert(point);

            MainActivity activity = MainActivity.instance;
            if (activity.points == null)
                activity.points =  new ArrayList<Point>();
            activity.points.add(point);
            if (activity.points.size() > TRAIL_SIZE_LOCATIONS) activity.points.remove(0);


            Intent broadcastIntent = new Intent();
            broadcastIntent.putExtra("location", currentLocation);
            broadcastIntent.putExtra("speed", currentSpeed);
            broadcastIntent.setAction(MapViewFragment.UpdateBroadcastReceiver.LOCATION_READY_ACTION);
            sendBroadcast(broadcastIntent);
        }
    }

    //Send the new intent of a new speed for the history graph
    void sendNewSpeedIntent() {

        MainActivity activity = MainActivity.instance;

        //update the queue
        if (activity != null) {
            activity.current_index += 1;
            activity.queueHisotry.add(new Entry(activity.current_index, currentSpeed));
            if (activity.queueHisotry.size() > TRAIL_SIZE) activity.queueHisotry.remove(0);
        }

        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra("speed", currentSpeed);
        System.out.print(currentSpeed);
        broadcastIntent.setAction(DriverFragment.UpdateBroadcastReceiver.LOCATION_READY_ACTION);
        sendBroadcast(broadcastIntent);
    }


    public void sendNewNotificationLimitExceeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;
        String address = "";
        try {
            addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0)
                address = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (controllingUser != null) { //otherwise none controlling you

            try {

                Log.d("debug", "LOCATION_CHANGE INVIO NOTIFICA");

                OneSignal.postNotification(
                        new JSONObject("{'contents': {'en':'Limite superato! " + currentSpeed + " Km/h'}, " +
                                "'include_player_ids': ['" + controllingUser.playerId + "']," +
                                "'data': {'speed': '" + currentSpeed + "', 'location': '" + address + "','timestamp': '" + System.currentTimeMillis() + "' }}"), null);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("debug", "UserId EXCEPTION: " + e.getMessage());

            }
        }

    }


    public void insertNewNotificationDB() {

        NotificationDBOpenHelper dbOpenHelper = new NotificationDBOpenHelper(this);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NotificationSpeedEntity.NotificationEntity.LIMIT, SPEED_LIMIT);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;
        String address = "";
        try {
            addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0)
                address = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        values.put(NotificationSpeedEntity.NotificationEntity.LOCATION, address);
        values.put(NotificationSpeedEntity.NotificationEntity.LIMIT, SPEED_LIMIT);
        values.put(NotificationSpeedEntity.NotificationEntity.SPEED, Math.round(currentSpeed));
        values.put(NotificationSpeedEntity.NotificationEntity.USER, "User1");
        values.put(NotificationSpeedEntity.NotificationEntity.TIME, System.currentTimeMillis() + "");

        long r = db.insert(NotificationSpeedEntity.NotificationEntity.TABLE, null, values);

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onProviderEnabled(String provider) {

        locationManager.requestLocationUpdates(provider, MIN_TIME_SAMPLE_LOCATION, MIN_DIST_SAMPLE_LOCATION, this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(this);
    }


    public class TrackingBinder extends Binder {
        LocationTrackingService getService() {
            return LocationTrackingService.this;
        }
    }


}
