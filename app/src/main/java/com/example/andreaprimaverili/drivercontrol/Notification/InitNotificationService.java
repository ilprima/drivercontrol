package com.example.andreaprimaverili.drivercontrol.Notification;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.example.andreaprimaverili.drivercontrol.Driver.MapTools.PointManager.TAG;

/**
 * Created by andreaprimaverili on 27/10/2018.
 */

//guida: https://www.codementor.io/flame3/send-push-notifications-to-android-with-firebase-du10860kb

public class InitNotificationService extends FirebaseInstanceIdService {

    /*
    The registration token may change when:
        The app deletes Instance ID
        The app is restored on a new device
        The user uninstalls/reinstall the app
        The user clears app data.
    */


    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();


        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }


    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }


}
