package com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB;

import android.provider.BaseColumns;

/**
 * Created by andreaprimaverili on 21/10/2018.
 */

public class UserRegisteredEntity {

    public static abstract class UserRegistered implements BaseColumns {
        public static final String TABLE = "userRegistered";
        public static final String LIMIT = "speedLimit";
        public static final String NAME = "name";
        public static final String PLAYERID = "playerId";
        public static final String CONTROLLED = "controlled";
    }

}
