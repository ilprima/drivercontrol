package com.example.andreaprimaverili.drivercontrol.Driver;

import android.Manifest;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.andreaprimaverili.drivercontrol.Driver.MapTools.Point;
import com.example.andreaprimaverili.drivercontrol.Driver.MapTools.PointManager;
import com.example.andreaprimaverili.drivercontrol.MainActivity;
import com.example.andreaprimaverili.drivercontrol.R;
import com.github.mikephil.charting.data.Entry;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by andreaprimaverili on 14/10/2018.
 */

public class MapViewFragment extends Fragment implements OnMapReadyCallback {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private static final float ZOOM_LEVEL = 19;
    public static final LatLng PISA_ING = new LatLng(43.721361, 10.389927);
    public GoogleMap gmap;

    private View myView;


    private UpdateBroadcastReceiver ubr;
    private IntentFilter filter;


    private PointManager pointManager;

    private List<Point> points;

    //store the current lines to be deleted
    private List<Polyline> currentPolylines = new ArrayList<Polyline>();

    private boolean isMapInitialized = false;

    private boolean disableAutoOrientation = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        ubr = new UpdateBroadcastReceiver();
        filter = new IntentFilter(UpdateBroadcastReceiver.LOCATION_READY_ACTION);
        pointManager = new PointManager();

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        myView = inflater.inflate(R.layout.map, container, false);

        //cerca se c'è il permesso della posizione
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            //return;

            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.request_location_permission)
                    .setMessage(R.string.request_location_permission2)
                    .setPositiveButton(R.string.request_location_permission_accept, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    })
                    .create()
                    .show();

            return myView;


        } else { //permesso c'è

            MapView mapView = myView.findViewById(R.id.gmapView);
            mapView.onCreate(savedInstanceState);
            mapView.onResume();

            MapsInitializer.initialize(getActivity().getApplicationContext());
            mapView.getMapAsync(this);
            //((MapFragment) getChildFragmentManager().findFragmentById(R.id.gmapView)).getMapAsync(this);

        }

        return myView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(ubr, filter);
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(ubr);
        super.onPause();

    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {

        gmap = googleMap;

        gmap.getUiSettings().setZoomControlsEnabled(true);
        gmap.setMyLocationEnabled(true);
        gmap.getUiSettings().setMyLocationButtonEnabled(true);


        //gmap.moveCamera(CameraUpdateFactory.newLatLng(PISA_ING));
        gmap.moveCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL));

        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            points = ((MainActivity) getActivity()).points;
            if (points != null && points.size()>0) {
                LatLng latLng = new LatLng(points.get(points.size() - 1).getLatitude(), points.get(points.size() - 1).getLongitude());
                gmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                gmap.animateCamera(CameraUpdateFactory.newLatLng(
                        new LatLng(points.get(points.size() - 1).getLatitude(), points.get(points.size() - 1).getLongitude())));
            }

        }
        isMapInitialized = true;


        gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                disableAutoOrientation = !disableAutoOrientation;
            }
        });


    }

    private void updateCameraBearing(GoogleMap googleMap, float bearing) {
        if (googleMap == null) return;
        CameraPosition camPos = CameraPosition
                .builder(
                        googleMap.getCameraPosition() // current Camera
                )
                .bearing(bearing)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
    }


    public class UpdateBroadcastReceiver extends BroadcastReceiver {

        public static final String LOCATION_READY_ACTION = "it.unipi.iet.LOCATION_READY";

        @Override
        public void onReceive(Context context, Intent intent) {

            if (isMapInitialized) {

                points = ((MainActivity) getActivity()).points;

                for (Polyline p : currentPolylines) {
                    p.remove();
                }
                currentPolylines.clear();

                gmap.clear();


                if (pointManager != null) {
                    for (int i = 0; i < points.size() - 2; i++) {
                        pointManager.insert(points.get(i));
                        pointManager.insert(points.get(i + 1));
                        gmap.clear();
                        PolylineOptions po = pointManager.getPolylineOptionStepByStep(points.get(i), points.get(i + 1), points.get(i + 2), getActivity());
                        currentPolylines.add(gmap.addPolyline(po));
                    }

                }

                LatLng latLng = new LatLng(points.get(points.size() - 1).getLatitude(), points.get(points.size() - 1).getLongitude());
                gmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                gmap.animateCamera(CameraUpdateFactory.newLatLng(
                        new LatLng(points.get(points.size() - 1).getLatitude(), points.get(points.size() - 1).getLongitude())));


                if (!disableAutoOrientation) {
                    updateCameraBearing(gmap, points.get(points.size() - 1).getBearing());
                }
            }

        }

    }


}
