package com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB;

import android.provider.BaseColumns;

/**
 * Created by andreaprimaverili on 21/10/2018.
 */

public class NotificationSpeedEntity {

    public static abstract class NotificationEntity implements BaseColumns {
        public static final String TABLE = "notificaiton";
        public static final String SPEED = "speed";
        public static final String LIMIT = "limitation";
        public static final String USER = "user";
        public static final String TIME = "time";
        public static final String LOCATION = "location";
    }

}
