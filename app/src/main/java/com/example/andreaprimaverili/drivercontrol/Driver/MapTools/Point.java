package com.example.andreaprimaverili.drivercontrol.Driver.MapTools;

/**
 * Created by andreaprimaverili on 16/10/2018.
 */

public class Point {


    private double latitude;
    private double longitude;
    private float accuracy;
    private long time;
    private String provider;
    private float speed;
    private float bearing;

    public Point(double lat, double lon, float acc, long time, String prov, float speed, float bearing) {
        this.latitude = lat;
        this.longitude = lon;
        this.accuracy = acc;
        this.provider = prov;
        this.speed = speed;
        this.bearing = bearing;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getProvider() {
        return provider;
    }

    public float getSpeed() { return speed; }
    public float getBearing() { return bearing; }

    public void setProvider(String provider) {
        this.provider = provider;
    }



}
