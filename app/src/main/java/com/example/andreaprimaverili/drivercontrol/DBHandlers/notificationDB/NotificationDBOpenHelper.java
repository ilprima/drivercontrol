package com.example.andreaprimaverili.drivercontrol.DBHandlers.notificationDB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by andreaprimaverili on 21/10/2018.
 */
public class NotificationDBOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "driverControl.db";
    private static final int VERSION = 1;
    private static final String DB_CREATE_TABLE = "create table " + NotificationSpeedEntity.NotificationEntity.TABLE + "("
            + NotificationSpeedEntity.NotificationEntity._ID + " INTEGER PRIMARY KEY, "
            + NotificationSpeedEntity.NotificationEntity.SPEED + " INTEGER, "
            + NotificationSpeedEntity.NotificationEntity.LIMIT + " INTEGER, "
            + NotificationSpeedEntity.NotificationEntity.LOCATION + " STRING, "
            + NotificationSpeedEntity.NotificationEntity.TIME + " STRING, "
            + NotificationSpeedEntity.NotificationEntity.USER + " STRING"
            + ")";

    public NotificationDBOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVers, int newVers) { // code for updating the database structure
        // Here, for simplicity, just drop and recreate
        db.execSQL("DROP TABLE IF EXISTS " + NotificationSpeedEntity.NotificationEntity.TABLE);
        onCreate(db);
    }


}