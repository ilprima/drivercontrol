package com.example.andreaprimaverili.drivercontrol.DBHandlers.userRegisteredDB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by andreaprimaverili on 21/10/2018.
 */
public class UserRegisteredDBOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "driverControlUsers.db";
    private static final int VERSION = 1;
    private static final String DB_CREATE_TABLE = "create table " + UserRegisteredEntity.UserRegistered.TABLE + "("
            + UserRegisteredEntity.UserRegistered._ID + " INTEGER PRIMARY KEY, "
            + UserRegisteredEntity.UserRegistered.PLAYERID + " STRING, "
            + UserRegisteredEntity.UserRegistered.NAME + " STRING, "
            + UserRegisteredEntity.UserRegistered.LIMIT + " INTEGER, "
            + UserRegisteredEntity.UserRegistered.CONTROLLED + " INTEGER"
            + ")";

    public UserRegisteredDBOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVers, int newVers) { // code for updating the database structure
        // Here, for simplicity, just drop and recreate
        db.execSQL("DROP TABLE IF EXISTS " + UserRegisteredEntity.UserRegistered.TABLE);
        onCreate(db);
    }


}