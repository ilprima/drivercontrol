Driver control: the app
Driver control is a parental control app, developed for Android devices, that allows a parent device to control the car speed of a child device.
Setting of the speed limit.
Can control one child device at time.
Push notification if the limit is exceeded.
Live tracking of current car speed (speedometer).
Controls also in background mode.